export class EmployeeModel{

    id :number=0;
    firstName :string ='';
    lastName :string ='';
    sap :string ='';
    email :string ='';
    mobile :string ='';
    salary :string ='';
}